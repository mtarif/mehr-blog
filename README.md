# Mehr Blog

Here is Mehr Blog HTML template! Built with Bootstrap, have RTL support, persian fonts and licensed under the gnu general public license version 3.

![index-page](https://assets.gitlab-static.net/mtarif/mehr-blog/raw/master/assets/images/screenshot.jpg)